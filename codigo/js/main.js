let cerrar= document.querySelectorAll(".close")[0];
let abrir= document.querySelectorAll(".in")[0];
let modal= document.querySelectorAll(".modal")[0];
let modalC= document.querySelectorAll(".modal-container")[0];
let ingresar= document.querySelectorAll(".button")[0];

ingresar.addEventListener("click",function(e){
    e.preventDefault();
    setTimeout(()=> location.href="principal.html",500);
});

abrir.addEventListener("click",function(e){
    e.preventDefault();
    modalC.style.opacity="1";
    modalC.style.visibility="visible";
    modal.classList.toggle("modal-close")
});

cerrar.addEventListener("click",function(){
    modal.classList.toggle("modal-close");
    setTimeout(function(){
        modalC.style.opacity="0";
        modalC.style.visibility="hidden";
    },1000)
});

window.addEventListener("click",function(e){
    console.log(e.target);
    if(e.target==modalC){
        modal.classList.toggle("modal-close");
        setTimeout(function(){
            modalC.style.opacity="0";
            modalC.style.visibility="hidden";
        },1000)
    }
});

window.onload = function(){$("#showPassword").hide();}

$("#txtPassword").on('change',function() {  
		if($("#txtPassword").val())
		{
			$("#showPassword").show();
		}
		else
		{
			$("#showPassword").hide();
		}
});

$(".reveal").on('click',function() {
    var $pwd = $("#txtPassword");
    if ($pwd.attr('type') === 'password') 
		{
        $pwd.attr('type', 'text');
    } 
		else 
		{
        $pwd.attr('type', 'password');
    }
});