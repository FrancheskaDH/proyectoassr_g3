//definicion de la constante app para la funcion express
const path = require('path')
const express = require('express');
const app = express();

//body-parser
app.use(express.urlencoded({extended:false}));
app.set('port', process.env.PORT | 3000);
app.get('/', (req,res)=>{
    res.sendFile(path.resolve(__dirname, '../vistas/registro.html'))
});

app.listen(app.get('port'),()=>{
    console.log(`Server on port ${app.get('port')}`);
});
