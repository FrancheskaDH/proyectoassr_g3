const {Router} = require("express");
const router = Router();
const{ getRegistro, addRegistro } = require("../controller/estudiante.controller");

router.get('/', getRegistro);
router.post("/", addRegistro);

module.exports = router;