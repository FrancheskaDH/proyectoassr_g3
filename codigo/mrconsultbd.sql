drop database if exists mrconsultdb;
create database mrconsultdb;
use mrconsultdb;

create table detalle_usuario (
    Nick varchar(20) primary key,
    Nombre_1 varchar(30) not null,
    Nombre_2 varchar(30) null,
    Apellido_1 varchar(30) not null,
    Apellido_2 varchar(30) null);
    
create table usuario (
	Nick varchar(20) primary key,
    Correo varchar(40) not null,
    Clave varchar(20) not null,
    foreign key(Nick) references detalle_usuario(Nick));

create table router (
	MAC char(12) primary key,
    Modelo varchar(20) null,
    Propietario varchar(20) not null,
    foreign key(Propietario) references usuario(Nick));

create table estado (
	idEstado int(5) primary key,
	MAC char(12) not null,
    CPU_ varchar(20) not null,
    Memoria varchar(20) not null,
    Trafico text not null,
    foreign key(MAC) references router(MAC));

create table historial (
	idConsulta int(5) primary key,
    idEstado int(5) not null,
    Comando_Consulta varchar(150) not null,
    Hora datetime not null,
    foreign key(idEstado) references estado(idEstado));
